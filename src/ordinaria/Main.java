package ordinaria;

/**
 *
 * @author nacho
 */
public class Main {

    
    public static void main(String[] args) {
        
    }
    
     static boolean esBisiesto(int anyo) {
        boolean esBisiesto=false;
        if (anyo%4==0) {
            esBisiesto=true;
            if (anyo%100==0) {
                esBisiesto=false;
                if (anyo%400==0) {
                    esBisiesto=true;
                }
            }
        }
        return esBisiesto;
    }
    
    static boolean esDniValido(String dni) {
        boolean esValido=false;
        String letras="TRWAGMYFPDXBNJZSQVHLCKE";
        //Primera comprobación es que tenga 9 caracteres en total
        if (dni.length()==9) {
            //Siguiente comprobación es que lo primero sean 8 dígitos
            boolean todoDigitos=true;
            String digitos="0123456789";
            String aux="";
            for (int i=0; i<8; i++) {
                if (digitos.indexOf(dni.charAt(i)+"")==-1){ //No dígito
                    todoDigitos=false;
                    break;
                } else {
                    aux+=dni.charAt(i);
                }
            }
            if (todoDigitos) {
                // Ahora hemos de comprobar que el último caracter es una
                // letra y además es la adecuada
                int resto=Integer.parseInt(aux)%23;
                String letraCalculada=letras.charAt(resto)+"";
                String letraDni=dni.charAt(8)+"";
                if (letraCalculada.equalsIgnoreCase(letraDni)) {
                    esValido=true;
                }
            }
        }
        return esValido;
    }
    
    static int diasMes(int mes, boolean esBisiesto) {
        int diasMes=0;
        if (mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes==10 || mes==12) {
            diasMes=31;
        } else if (mes==4 || mes==6 || mes==9 || mes==11) {
            diasMes=30;
        } else if (mes==2) {
            if (esBisiesto) {
                diasMes=29;
            } else {
                diasMes=28;
            }
        } 
        return diasMes;
    }
}
