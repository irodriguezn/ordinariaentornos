
package ordinaria;


/**
 *
 * @author nacho
 */
public class Mates {
    private int op1;
    private int op2;

    public Mates(int op1, int op2) {
        this.op1 = op1;
        this.op2 = op2;
    }
    
    public double media() {
        double suma=op1+op2;
        return suma/2;
    }


    public int mcd() {
        int mayor, menor, resultado=0, resto;
        if (op1>=op2) {
            mayor=op1;
            menor=op2;
        } else {
            mayor=op2;
            menor=op1;
        }
        boolean salir=false;
        while (!salir) {
            resto=mayor%menor;
            if (resto==0) {
                resultado=menor;
                break;
            }
            mayor=menor;
            menor=resto;
        }
        return resultado;
    }
    
    public int mcm() {
        int mcd=mcd();
        return (op1*op2)/mcd;
    }
}
